<?php

namespace Luma\Superheroes\Api;

use Magento\Framework\Exception\LocalizedException;

interface SuperheroRepositoryInterface
{
    public function save(\Luma\Superheroes\Api\Data\Superhero $superhero);

    /**
     * @param int $id
     * @return mixed
     * @throws LocalizedException
     */
    public function get(int $id);

    public function getAll();

    public function delete(\Luma\Superheroes\Api\Data\Superhero $superhero);
}