<?php

namespace Luma\Superheroes\Api\Data;

interface Superhero
{
    public function save();

    public function delete();
}