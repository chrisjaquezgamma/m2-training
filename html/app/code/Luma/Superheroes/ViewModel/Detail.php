<?php

namespace Luma\Superheroes\ViewModel;

use Luma\Superheroes\Api\SuperheroRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;

class Detail implements \Magento\Framework\View\Element\Block\ArgumentInterface {

    /** @var SuperheroRepositoryInterface  */
    protected $superheroRepository;

    /** @var \Magento\Framework\App\RequestInterface  */
    protected $request;

    public function __construct(
        SuperheroRepositoryInterface $superheroRepository,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->superheroRepository = $superheroRepository;

        $this->request = $request;
    }

    public function getHero() {
        $id = $this->getRequestId();

        if($id) {
            try {
                return $this->superheroRepository->get($id);
            } catch (LocalizedException $e) {
                // TODO: Add cameo for unknown superhero
            }
        }

        return null;
    }

    protected function getRequestId()
    {
        return $this->request->getParam('hero');
    }
}