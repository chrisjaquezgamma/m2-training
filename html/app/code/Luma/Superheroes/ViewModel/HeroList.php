<?php

namespace Luma\Superheroes\ViewModel;

use Luma\Superheroes\Api\SuperheroRepositoryInterface;

class HeroList implements \Magento\Framework\View\Element\Block\ArgumentInterface {

    /** @var SuperheroRepositoryInterface  */
    protected $superheroRepository;

    /** @var \Magento\Framework\App\RequestInterface  */
    protected $request;

    public function __construct(
        SuperheroRepositoryInterface $superheroRepository,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->superheroRepository = $superheroRepository;
        $this->request = $request;
    }

    public function getList() {
        return $this->superheroRepository->getAll();
    }

    public function getActiveClass(int $id) {
        if($id == $this->getRequestId()) {
            return 'active';
        }

        return '';
    }

    protected function getRequestId()
    {
        return $this->request->getParam('hero');
    }
}