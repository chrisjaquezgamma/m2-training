<?php

namespace Luma\Superheroes\Model\ResourceModel\Superhero;

class Collection extends \Magento\Eav\Model\Entity\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            \Luma\Superheroes\Model\Superhero::class,
            \Luma\Superheroes\Model\ResourceModel\Superhero::class
        );
    }
}
