<?php

namespace Luma\Superheroes\Model\ResourceModel;

use Magento\Eav\Model\Entity\Context;

class Superhero extends \Magento\Eav\Model\Entity\AbstractEntity
{
    protected function _construct()
    {
        $this->connectionName = \Luma\Superheroes\Model\Superhero::ENTITY;
    }

    public function getEntityType()
    {
        if (empty($this->_type)) {
            $this->setType(\Luma\Superheroes\Model\Superhero::ENTITY);
        }
        return parent::getEntityType();
    }
}
