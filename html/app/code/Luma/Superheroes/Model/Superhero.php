<?php

namespace Luma\Superheroes\Model;

class Superhero extends \Magento\Framework\Model\AbstractModel implements \Luma\Superheroes\Api\Data\Superhero
{
    const ENTITY = "luma_superheroes_superhero";

    protected function _construct()
    {
        $this->_init(\Luma\Superheroes\Model\ResourceModel\Superhero::class);
    }
}
