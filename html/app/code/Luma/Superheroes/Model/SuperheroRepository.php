<?php

namespace Luma\Superheroes\Model;

use Luma\Superheroes\Model\ResourceModel\Superhero\Collection;
use Magento\Framework\Exception\LocalizedException;

class SuperheroRepository implements \Luma\Superheroes\Api\SuperheroRepositoryInterface
{
    protected $superheroes = null;

    protected $superheroCollectionFactory;

    public function __construct(\Luma\Superheroes\Model\ResourceModel\Superhero\CollectionFactory $superheroCollectionFactory)
    {
        $this->superheroCollectionFactory = $superheroCollectionFactory;
    }

    public function save(\Luma\Superheroes\Api\Data\Superhero $superhero)
    {
        $superhero->save();
    }

    /**
     * @param int $id
     * @return mixed
     * @throws LocalizedException
     */
    public function get(int $id)
    {
        if($this->superheroes === null) {
            $this->loadSuperheroes();
        }

        return $this->getById($id);
    }

    public function getAll()
    {
        if($this->superheroes === null) {
            $this->loadSuperheroes();
        }

        return $this->superheroes;
    }

    public function delete(\Luma\Superheroes\Api\Data\Superhero $superhero)
    {
        $superhero->delete();
    }

    protected function loadSuperheroes()
    {
        /** @var Collection $superheroCollection */
        $superheroCollection = $this->superheroCollectionFactory->create();

        $superheroCollection->addAttributeToSelect('*');

        $this->superheroes = [];

        foreach ($superheroCollection as $superhero) {
            $this->superheroes[$superhero->getEntityId()] = $superhero;
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws LocalizedException
     */
    protected function getById(int $id)
    {
        if($this->superheroes === null) {
            $this->loadSuperheroes();
        }

        if(array_key_exists($id, $this->superheroes)) {
            return $this->superheroes[$id];
        }

        throw new LocalizedException(__("Unable to find superhero with id %s", $id));
    }


}