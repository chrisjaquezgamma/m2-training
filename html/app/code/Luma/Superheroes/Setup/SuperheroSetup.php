<?php

namespace Luma\Superheroes\Setup;

use Magento\Eav\Setup\EavSetup;

class SuperheroSetup extends EavSetup
{
    public function getDefaultEntities()
    {
        $superheroEntity = 'luma_superheroes_superhero';
        $entities = [
            $superheroEntity => [
                'entity_model' => 'Luma\Superheroes\Model\ResourceModel\Superhero',
                'table' => $superheroEntity . '_entity',
                'attributes' => [
                    'superhero_name' => [
                        'type' => 'static',
                    ],
                    'real_name' => [
                        'type' => 'static',
                    ],
                    'superpower' => [
                        'type' => 'static',
                    ],
                    'gender' => [
                        'type' => 'static',
                    ],
                    'nationality' => [
                        'type' => 'static',
                    ],
                ],
            ],
        ];
        return $entities;
    }
}
