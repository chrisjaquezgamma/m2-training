<?php

namespace Luma\Superheroes\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    private $superheroSetupFactory;
    protected $superheroFactory;
    protected $superheroCollectionFactory;

    public function __construct(
        \Luma\Superheroes\Setup\SuperheroSetupFactory $superheroSetupFactory,
        \Luma\Superheroes\Model\SuperheroFactory $superheroFactory,
        \Luma\Superheroes\Model\ResourceModel\Superhero\CollectionFactory $superheroCollectionFactory
    )
    {
        $this->superheroSetupFactory = $superheroSetupFactory;
        $this->superheroFactory = $superheroFactory;
        $this->superheroCollectionFactory = $superheroCollectionFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $superheroEntity = \Luma\Superheroes\Model\Superhero::ENTITY;

        /** @var EavSetup $superheroSetup */
        $superheroSetup = $this->superheroSetupFactory->create(['setup' => $setup]);

        /*
         * adding entity to entity_type table and eav attributes to eav_attribute table
         */
        $superheroSetup->installEntities();

        $superheroSetup->addAttribute(
            $superheroEntity, 'race', ['type' => 'varchar']
        );

        $superheroSetup->addAttribute(
            $superheroEntity, 'favorite_food', ['type' => 'varchar']
        );

        $superheroSetup->addAttribute(
            $superheroEntity, 'favorite_artistic_movement', ['type' => 'varchar']
        );

        $superheroSetup->addAttribute(
            $superheroEntity, 'alignment', ['type' => 'varchar']
        );

        $superheroSetup->addAttribute(
            $superheroEntity, 'age', ['type' => 'int']
        );

        $superheroSetup->addAttribute(
            $superheroEntity, 'height', ['type' => 'int']
        );

        $superheroSetup->addAttribute(
            $superheroEntity, 'suit_color', ['type' => 'varchar']
        );

        $superheroSetup->addAttribute(
            $superheroEntity, 'favorite_yoga_pose', ['type' => 'varchar']
        );

        $superheroSetup->addAttribute(
            $superheroEntity, 'superpower_side_effects', ['type' => 'varchar']
        );

        $superheroes = [
            [
                'name' => 'Muffin Man',
                'real_name' => 'Joseph Smith',
                'superpower' => 'Heat Waves',
                'gender' => 'Male',
                'nationality' => 'American',
                'race' => 'African American',
                'food' => 'Chocolate Muffins',
                'artistic_movement' => 'Baroque',
                'alignment' => 'Lawful Neutral',
                'age' => 32,
                'height' => 195,
                'suit_color' => 'Chocolate',
                'yoga_pose' => 'Downward Facing Dog',
                'side_effects' => 'Sometimes he sets his environments on fire'
            ],
            [
                'name' => 'Lady Winter',
                'real_name' => 'Ashley Kane',
                'superpower' => 'Freeze on touch',
                'gender' => 'Female',
                'nationality' => 'Canadian',
                'race' => 'European',
                'food' => 'Vegetarian Meatballs',
                'artistic_movement' => 'Romanticism',
                'alignment' => 'Lawful Good',
                'age' => 21,
                'height' => 168,
                'suit_color' => 'Pearl White',
                'yoga_pose' => 'Lotus Pose',
                'side_effects' => "Can't touch anything without freezing it"
            ],
            [
                'name' => 'Thunderstorm',
                'real_name' => 'Carla Marina',
                'superpower' => 'Can accelerate to light speed',
                'gender' => 'Female',
                'nationality' => 'Mexican',
                'race' => 'Hispanic',
                'food' => 'Beer',
                'artistic_movement' => 'Futurism',
                'alignment' => 'Neutral Good',
                'age' => 28,
                'height' => 165,
                'suit_color' => 'Metallic Black',
                'yoga_pose' => 'Warrior II',
                'side_effects' => 'Going too fast can transform the atmosphere into plasma'
            ],
            [
                'name' => 'The False Prophet',
                'real_name' => 'Milorad Močić',
                'superpower' => 'Omnipresence',
                'gender' => 'Male',
                'nationality' => 'Serbian',
                'race' => 'Kosovar',
                'food' => 'Sheep Barbecue',
                'artistic_movement' => 'Expressionism',
                'alignment' => 'Chaotic Good',
                'age' => 48,
                'height' => 179,
                'suit_color' => 'Shining White',
                'yoga_pose' => 'Half Lord of the Fishes',
                'side_effects' => 'Religious people have started to adore him as a god'
            ],
            [
                'name' => 'Master Sensei',
                'real_name' => 'Lewis Roll',
                'superpower' => 'Super strength and metabolism',
                'gender' => 'Male',
                'nationality' => 'British',
                'race' => 'Briton',
                'food' => 'Gyros',
                'artistic_movement' => 'Neoclassicism',
                'alignment' => 'Lawful Neutral',
                'age' => 23,
                'height' => 170,
                'suit_color' => 'Blood Red',
                'yoga_pose' => 'Mountain Tadasana',
                'side_effects' => 'Requires 10000 kcal per day to maintain this shape'
            ],
            [
                'name' => 'The Mime',
                'real_name' => 'Unknown',
                'superpower' => 'Can summon and manipulate any kind of objects',
                'gender' => 'Unknown',
                'nationality' => 'Unknown',
                'race' => 'Unknown',
                'food' => 'Pho',
                'artistic_movement' => 'Abstract Impressionism',
                'alignment' => 'True Neutral',
                'age' => 191,
                'height' => 220,
                'suit_color' => 'Black and White with blue stripes',
                'yoga_pose' => 'Headstand',
                'side_effects' => 'Decreases the entropy of the universe'
            ]
        ];
        foreach ($superheroes as $super) {
            $superhero = $this->superheroFactory->create();
            $superhero->setSuperheroName($super['name'])
                ->setRealName($super['real_name'])
                ->setSuperpower($super['superpower'])
                ->setGender($super['gender'])
                ->setNationality($super['nationality'])
                ->setRace($super['race'])
                ->setFavoriteFood($super['food'])
                ->setFavoriteArtisticMovement($super['artistic_movement'])
                ->setAlignment($super['alignment'])
                ->setAge($super['age'])
                ->setHeight($super['height'])
                ->setSuitColor($super['suit_color'])
                ->setFavoriteYogaPose($super['yoga_pose'])
                ->setSuperpowerSideEffects($super['side_effects'])
                ->save();
        }


        $setup->endSetup();
    }
}