<?php

namespace Luma\Superheroes\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class UpgradeData implements UpgradeDataInterface
{
    private $superheroSetupFactory;
    protected $superheroFactory;
    protected $superheroCollectionFactory;

    public function __construct(
        \Luma\Superheroes\Setup\SuperheroSetupFactory $superheroSetupFactory,
        \Luma\Superheroes\Model\SuperheroFactory $superheroFactory,
        \Luma\Superheroes\Model\ResourceModel\Superhero\CollectionFactory $superheroCollectionFactory
    )
    {
        $this->superheroSetupFactory = $superheroSetupFactory;
        $this->superheroFactory = $superheroFactory;
        $this->superheroCollectionFactory = $superheroCollectionFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $superheroEntity = \Luma\Superheroes\Model\Superhero::ENTITY;

        /** @var EavSetup $superheroSetup */
        $superheroSetup = $this->superheroSetupFactory->create(['setup' => $setup]);

        if (version_compare($context->getVersion(),'1.0.5','<')) {
            $superheroSetup->addAttribute(
                $superheroEntity, 'favorite_product', ['type' => 'varchar']
            );

            $superheroSetup->addAttribute(
                $superheroEntity, 'catchphrase', ['type' => 'varchar']
            );

            $superheroes = [
                [
                    'name' => "Muffin Man",
                    'product' => 'MH09',
                    'catchphrase' => 'Nothing sweeter than victory!'
                ],
                [
                    'name' => 'Lady Winter',
                    'product' => 'WH05',
                    'catchphrase' => "Can't defeat my ice will"
                ],
                [
                    'name' => 'Thunderstorm',
                    'product' => '24-WG02',
                    'catchphrase' => 'No match for me, casero!'
                ],
                [
                    'name' => 'The False Prophet',
                    'product' => '240-LV07',
                    'catchphrase' => 'Kneel before your God!'
                ],
                [
                    'name' => 'Master Sensei',
                    'product' => '24-WG080',
                    'catchphrase' => 'According to the plan'
                ],
                [
                    'name' => 'The Mime',
                    'product' => '24-MB06',
                    'catchphrase' => 'Predictable!'
                ]
            ];

            foreach ($superheroes as $superhero) {
                $collection = $this->superheroCollectionFactory->create()
                    ->addAttributeToFilter(
                        'superhero_name',
                        ['eq' => $superhero['name']]
                    );
                if ($collection->getSize() > 0) {
                    $item = $collection->getFirstItem();
                    $item->setFavoriteProduct($superhero['product'])
                        ->setCatchphrase($superhero['catchphrase'])
                        ->save();
                }
            }
        }

        $setup->endSetup();
    }
}
